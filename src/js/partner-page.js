import { DOM } from './helpers/_consts';
import scroller from './modules/scroller/_scrollerSimplePage';
import initGallery from './modules/_imgGallery';
import initPartnerSlider from './modules/_partnerSlider';
import initBurger from './modules/_burger';
import initHeaderNav from './modules/_header-nav';
import Player from './modules/_player';
import {clearNewsStorage, clearTopicsStorage} from './modules/_clearStorage';

const handleWinReady = () => {
  clearNewsStorage();
  clearTopicsStorage();

  initGallery();
  initPartnerSlider();
  scroller.init();
  initBurger(scroller);
  initHeaderNav();
  $('.js-player').each((i, el) => {
    // eslint-disable-next-line
    const player = new Player($(el));
  });
};

DOM.$win.ready(handleWinReady);
