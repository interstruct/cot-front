import scroller from './modules/scroller/_scrollerContactPage';
import { DOM } from './helpers/_consts';
import initBurger from './modules/_burger';
import initHeaderNav from './modules/_header-nav';
import {clearNewsStorage, clearTopicsStorage} from './modules/_clearStorage';

const handleWinReady = () => {
  clearNewsStorage();
  clearTopicsStorage();

  scroller.init();
  initBurger(scroller);
  initHeaderNav();

};

DOM.$win.ready(handleWinReady);
