import initHeroRandomizer from './modules/_heroRandomizer';
import initHeroVideo from './modules/_heroVideo';
import scroller from './modules/scroller/_scrollerHomePage';
import initSlider from './modules/_latestUpdatesSlider';
import initBurger from './modules/_burger';
import initHeaderNav from './modules/_header-nav';
import initModals from './modules/_modal';
import initHeroMask from './modules/_heroMask';
import {clearNewsStorage, clearTopicsStorage} from './modules/_clearStorage';
import { DOM } from './helpers/_consts';

const handleWinReady = () => {
  clearNewsStorage();
  clearTopicsStorage();

  initHeroRandomizer();
  initHeroVideo();
  scroller.init();
  initHeroMask();
  initBurger(scroller);
  initHeaderNav();
  initSlider();
  initModals(scroller);
};

DOM.$win.ready(handleWinReady);




