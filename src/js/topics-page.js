import scroller from './modules/scroller/_scrollerTopicsPage';
import { DOM } from './helpers/_consts';
import initBurger from './modules/_burger';
import initHeaderNav from './modules/_header-nav';
import {clearNewsStorage} from './modules/_clearStorage';
import initFilter from './modules/_filter';

const handleWinReady = () => {

  setTimeout(() => {
    clearNewsStorage();

    scroller.init(true);
    initFilter(scroller, 'filterTopicsActive', true)
    initBurger(scroller)
    initHeaderNav();
  }, 150)
};

DOM.$win.ready(handleWinReady);

