import 'babel-polyfill';
import 'object-fit-polyfill';
import svg4everybody from 'svg4everybody';
import { throttle, debounce } from 'throttle-debounce';

import './helpers/_detectBrowser';
import updateViewportUnits from './helpers/_updateViewPortUnits';
import initMsScroll from './modules/_msScroll';
import updateModalViewportUnits from './helpers/_udateModalViewportUnits';
import initInputAnimationsState from './modules/_inputStateAnimation';
import {initNavFilterBtns} from './modules/_filterBtn'

import { CLASSES, DOM, IS_TOUCH } from './helpers/_consts';

initMsScroll();

const initHelpers = () => {
  updateViewportUnits();
  updateModalViewportUnits();
  svg4everybody();
}

const initModules = () => {
  initInputAnimationsState();
}

const checkIsTouch = () => {
  const { touch, noTouch } = CLASSES;

  if (IS_TOUCH) {
    DOM.$body.addClass(touch);
  } else {
    DOM.$body.addClass(noTouch);
  }
};

const onResize = () => {
  updateViewportUnits();
}

const handleWinReady = () => {
  checkIsTouch();
  initHelpers();
  initModules();
  initNavFilterBtns();
};


DOM.$win.ready(handleWinReady);

if(IS_TOUCH){
  DOM.$win.on('orientationchange', debounce(450, onResize));
}else{
  DOM.$win.resize(throttle(300, onResize));
};

DOM.$win.resize(debounce(450, () => {updateModalViewportUnits()}));


