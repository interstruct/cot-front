import scroller from './modules/scroller/_scrollerAboutPage';
import initGoalSlider from './modules/_goalSlider';
import initIconsAnimation from './modules/_iconsAnimation';
import principlesInit from './modules/_principles';
import { DOM } from './helpers/_consts';
import initBurger from './modules/_burger';
import initHeaderNav from './modules/_header-nav';
import initModals from './modules/_modal';
import {clearNewsStorage, clearTopicsStorage} from './modules/_clearStorage';

const handleWinReady = () => {
  clearNewsStorage();
  clearTopicsStorage();

  scroller.init();
  initBurger(scroller);
  initHeaderNav();
  initGoalSlider();
  initIconsAnimation();
  principlesInit();
  initModals(scroller);
}

DOM.$win.ready(handleWinReady);



