import scroller from './modules/scroller/_scrollerSimplePage';
import { DOM } from './helpers/_consts';
import initBurger from './modules/_burger';
import initHeaderNav from './modules/_header-nav';
import initGallery from './modules/_imgGallery';
import Player from './modules/_player';
import {clearNewsStorage, clearTopicsStorage} from './modules/_clearStorage';

const handleWinReady = () => {
    clearNewsStorage();
    clearTopicsStorage();

    scroller.init();
    initBurger(scroller);
    initHeaderNav();
    initGallery();

    $('.js-player').each((i, el) => {
      // eslint-disable-next-line
      const player = new Player($(el));
    });
};

DOM.$win.ready(handleWinReady);



