import isTouch from './_detectTouch';

const { detect } = require('detect-browser');

const browser = detect();
export const DOM = {
  $win: $(window),
  $body: $('body'),
  $doc: $('document'),
  $header: $('.js-header'),
  $headerWrapper: $('.js-header-wrapper'),
  $htmlBody: $('html, body'),
  $nav: $('.js-nav'),
  $out: $('.js-out'),
  $bgBlock: $('.js-fixed-bg-block')
};  

export const CLASSES = {
  active: 'is-active',
  scrolled: 'is-scrolled',
  fixed: 'is-fixed',
  visible: 'is-visible',
  show: 'is-show',
  sent: 'is-sent',
  required: 'is-required',
  invalid: 'is-not-valid',
  touch: 'is-touch',
  noTouch: 'no-touch',
  overflowed: 'is-overflowed',
  menu: 'is-menu-opened',
  navScrolled: 'is-nav-scrolled',
  empty: 'is-empty',
  white: 'is-white',
  blue: 'is-blue'
};

export const GLOBAL = {
  browser: browser.name,
  os: browser.os
}
export const SCROLL_POS = {
  y: 0
};

export const BREAKPOINTS = {
  desktop: 1239,
  tablet: 959,
  mobile: 719
};

export const BIG_SCREEN_HEIGHT = 1100;

export const IS_TOUCH = isTouch();
export const MAX_PHONE_SIZE = 900;

let isPhone = false;
if(IS_TOUCH === true && window.innerWidth < MAX_PHONE_SIZE &&  window.innerHeight < MAX_PHONE_SIZE ){
 isPhone = true;
};
export const IS_PHONE = isPhone;