import { DOM } from "./_consts";
import calcHeaderStickyHeight from './_calcHeaderStickyHeight';
import  './_easings';

const MIN_DURATION = 0.5;
const MAX_DURATION = 2.5;

export default (target, calcOffset = true, calcPadddingTop = true, scrollDuration, minDur=false) => {
  if(!target[0]) return;

  const offset = calcOffset ? 50 : 0;
  const targetPaddingTop = calcPadddingTop ? parseFloat(target.css('padding-top')) : 0;
  const pageScroll = window.pageYOffset;
  const targetOffsetTop = target.offset().top;
  const headerOffset = pageScroll < targetOffsetTop ? 0 : calcHeaderStickyHeight();
  const targetPos = targetOffsetTop  + targetPaddingTop - headerOffset  - offset;
  const distance = Math.abs(pageScroll - targetPos);
  const pageHeight = DOM.$win.innerHeight();

  if(distance === 0) return;

  let duration = (distance/pageHeight)*0.4;
  if(duration < MIN_DURATION) duration = MIN_DURATION;
  if(duration > MAX_DURATION) duration = MAX_DURATION;

  if(scrollDuration) duration = scrollDuration;

  if(minDur) duration = duration < minDur ? minDur : duration

  DOM.$htmlBody.animate({
    scrollTop: targetPos
  }, duration*1000, 'easeInOutCubic');
}

