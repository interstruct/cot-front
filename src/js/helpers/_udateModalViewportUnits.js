export default () => {
  document.body.style.setProperty('--vh-modal', `${window.innerHeight}px`);
  document.body.style.setProperty('--vw-modal', `${window.innerWidth}px`);
};