export default () => {
  document.body.style.setProperty('--vh', `${window.innerHeight}px`);
  document.body.style.setProperty('--vw', `${window.innerWidth}px`);
};