
const isPortrait = () =>  {
   return !!window.matchMedia("(orientation: portrait)").matches
};

export default isPortrait;