import { BIG_SCREEN_HEIGHT, DOM } from "./_consts";

const isBigScreen = () =>  {
  return DOM.$win.innerHeight() >= BIG_SCREEN_HEIGHT; 
};

export default isBigScreen;