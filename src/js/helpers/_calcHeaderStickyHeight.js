import { DOM, BREAKPOINTS } from './_consts';

export default () => {
  const windowW = DOM.$win.innerWidth();
  let headerH = 0;
  if(windowW > BREAKPOINTS.mobile){
    headerH = windowW > BREAKPOINTS.desktop ? 88: 80;
  }else{
    headerH = 48;
  }
  return headerH;
}
