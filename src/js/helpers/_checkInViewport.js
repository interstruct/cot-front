import { DOM } from './_consts';
import calcHeaderStickyHeight from './_calcHeaderStickyHeight';

const inViewport = (elem, direction = 'down') =>  {
  const directionOffset = direction === 'down' ? 0 : calcHeaderStickyHeight();
  const elementTop = elem.offset().top - directionOffset;
  const elementBottom = elementTop + elem.outerHeight();
  const viewportTop = DOM.$win.scrollTop();
  const viewportBottom = viewportTop + DOM.$win.height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

export default inViewport;