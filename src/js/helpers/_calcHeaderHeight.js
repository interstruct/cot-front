import { DOM, BREAKPOINTS } from './_consts';

export default () => {
  const windowW = DOM.$win.innerWidth();
  let headerH = 0;
  if(windowW > BREAKPOINTS.mobile){
    headerH = windowW > BREAKPOINTS.desktop ? 176: 154;
  }else{
    headerH = 104;
  }
  return headerH;
}
