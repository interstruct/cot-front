import { throttle, debounce } from 'throttle-debounce';
import ScrollMagic from 'scrollmagic';

import initHeaderScrolling from './modules/scroller/_outScene';
import initHeroVideo from './modules/_heroVideo';
import heroCanvas from './modules/_heroCanvas';
import { DOM, IS_TOUCH } from './helpers/_consts';

class Scroller {
  constructor() {
    this.controller = new ScrollMagic.Controller();
  }

  init(){
    initHeaderScrolling(this.controller)
  }
};

const scroller = new Scroller();
const handleWinReady = () => {
  initHeroVideo();
  heroCanvas.init();
  scroller.init();
}

const onResize = () => {
  heroCanvas.resize();
};

DOM.$win.ready(handleWinReady);
if (IS_TOUCH) {
  DOM.$win.on('orientationchange', debounce(450, onResize));
} else {
  DOM.$win.resize(throttle(300, onResize));
};