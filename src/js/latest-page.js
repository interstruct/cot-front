import scroller from './modules/scroller/_scrollerLatestPage';
import initFilter from './modules/_filter';
import { DOM } from './helpers/_consts';
import initBurger from './modules/_burger';
import initHeaderNav from './modules/_header-nav';
import initModals from './modules/_modal';
import {clearTopicsStorage} from './modules/_clearStorage';

const handleWinReady = () => {
  setTimeout(() => {
    clearTopicsStorage();

    scroller.init();
    initFilter(scroller, 'filterNewsActive');
    initBurger(scroller);
    initHeaderNav();
    initModals(scroller);
  }, 150)
};

DOM.$win.ready(handleWinReady);



