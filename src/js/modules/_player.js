import Plyr from 'plyr';

import { GLOBAL } from '../helpers/_consts';

class Player {
  constructor(el) {
    this.$el = el;
    this.player = null;
    this.init();
  }

  init(){
    if(GLOBAL.browser === 'ie') return;
    this.player = new Plyr(this.$el, {
      ratio: '16:9',
      controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'fullscreen']
    });
  
    this.player.once('canplay', () => {this.player.currentTime = 0});
  }

  destroy(){
    this.player.destroy();
  }
}

export default Player;
// let player;
// export const initPlayer = () => {
//   if(GLOBAL.browser === 'ie') return;
//   player = new Plyr('.js-player', {
//     ratio: '16:9',
//     controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'volume', 'fullscreen']
//   });

//   player.once('canplay', () => {player.currentTime = 0});
// };

// export const destroyPlayer = () => {
//   player.destroy();
// };

// export { initPlayer, destroyPlayer };