import { DOM, BREAKPOINTS } from '../helpers/_consts';

const linesSpacer = 80;
const circlesRadius = 60;
const circleMaskRadius = 450;
const dpi = 2;
let prevDate = 0;
const timeDelay = 10;

class HeroCanvas {
  constructor() {
    this.$wrap = $('.js-hero-canvas-wrapper');
    this.wrap = document.querySelector('.js-hero-canvas-wrapper');
  }

  init() {
    if (!this.$wrap[0]) return;
    this.calcScrollPos();
    this.createCanvas();
    this.calcElementsValues();
    this.render();
  }

  createCanvas() {
    this.time = 0;
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.calcGlobalValues();
    this.setCanvasSize();
    this.$wrap.append(this.canvas);
  }

  calcScrollPos() {
    this.scrollTop = window.pageYOffset * dpi;
  }

  calcGlobalValues() {
    this.windowH = 1200;
    this.windowW = DOM.$win.innerWidth();
    this.canvasNatureWidth = this.windowW;
    this.canvasNatureHeight = this.windowH;
    this.canvasW = dpi * this.canvasNatureWidth;
    this.canvasH = dpi * this.canvasNatureHeight;
  }

  setCanvasSize() {
    this.canvas.width = this.canvasW;
    this.canvas.height = this.canvasH;
    this.canvas.style.width = `${this.canvasNatureWidth}px`;
    this.canvas.style.height = `${this.canvasNatureHeight}px`;
  }

  calcElementsValues() {
    this.linesCount = this.canvasNatureWidth > 2500 ? 70 : 50;
    this.calcLineWidth();
    this.calcCirclesValues();
    this.calcCircleMaskValues();
    this.calcNavbarClearMask();
    this.calcMainClearMask();
  }

  calcNavbarClearMask() {
    const $headerMenu = $('.js-header-menu');
    const navOffsetLeft = parseInt($headerMenu.offset().left * dpi, 10);
    const navOffsetTop = parseInt($headerMenu.offset().top * dpi, 10);
    this.navbarClearCoords = {
      x: this.canvasW - (this.canvasW - navOffsetLeft + 100),
      y: navOffsetTop + $headerMenu.innerHeight() + 70 * dpi
    }
  }

  calcMainClearMask() {
    const $headerContainer = $('.js-header-container');
    const containerOffsetLeft = parseInt($headerContainer.offset().left * dpi, 10);
    const headerContainerW = $headerContainer.innerWidth() * dpi;
    const needdedColsWidth = 21;
    const colsCount = this.windowW > BREAKPOINTS.mobile ? 24 : 12;
    const colW = headerContainerW / colsCount;
    const int = parseInt(containerOffsetLeft + (needdedColsWidth * colW), 10)
    this.mainClearCoords = {
      x: this.canvasW - (this.canvasW - int),
      y: this.navbarClearCoords.y
    };
  }

  calcCircleMaskValues() {
    this.circleMaskCenterPos = {
      x: this.canvasW / dpi + 1400,
      y: this.canvasH / dpi + 400
    }
  }

  calcLineWidth() {
    this.lineW = Math.sqrt(this.canvasW * this.canvasW + this.canvasH * this.canvasH) + 10;
  }

  calcCirclesValues() {
    this.circlesCount = 20;
    this.circlesCenterPos = {
      x: this.canvasW / dpi + 2000,
      y: this.canvasH / dpi + 1000
    }
  }

  drawLines(thickness) {
    this.ctx.strokeStyle = '#c8c952';
    this.ctx.lineWidth = thickness;
    for (let i = -this.linesCount; i < this.linesCount; i++) {
      this.ctx.beginPath();
      this.ctx.moveTo(0, (linesSpacer * dpi) * i);
      this.ctx.lineTo(this.lineW, (linesSpacer * dpi) * i + this.lineW);
      this.ctx.stroke();
    }
  }

  drawCircles(thickness) {
    this.ctx.save();

    this.ctx.strokeStyle = '#04c4dc';
    this.ctx.lineWidth = thickness;

    this.ctx.save();
    this.ctx.translate(this.circlesCenterPos.x - (circlesRadius * dpi), this.circlesCenterPos.y - (circlesRadius * dpi));
    for (let i = 1; i < this.circlesCount; i++) {
      this.ctx.beginPath();
      this.ctx.arc(0, 0, i * (circlesRadius * dpi), 0, 2 * Math.PI);
      this.ctx.stroke();
    }

    this.ctx.restore();
    this.ctx.restore();
  }

  render() {
    window.requestAnimationFrame(this.render.bind(this));

    const date = Date.now();
    if (date - prevDate < timeDelay) return;
    prevDate = date;
    this.calcScrollPos();

    this.ctx.clearRect(0, 0, this.canvasW, this.canvasH);
    this.ctx.fillStyle = "rgba(0, 0, 200, 0)";
    this.ctx.fillRect(0, 0, this.canvasW, this.canvasH);

    this.ctx.save();
    this.drawCircles(1 * dpi);
    this.drawLines(1 * dpi);

    this.ctx.beginPath();
    this.ctx.arc(this.circleMaskCenterPos.x, this.circleMaskCenterPos.y + this.scrollTop, circleMaskRadius * dpi, 0, Math.PI * 2);
    this.ctx.clip();

    this.drawCircles(10 * dpi);
    this.drawLines(7 * dpi);

    this.ctx.restore();
    this.ctx.clearRect(0, 0, this.navbarClearCoords.x, this.navbarClearCoords.y + this.scrollTop);
    this.ctx.clearRect(0, this.mainClearCoords.y + this.scrollTop, this.mainClearCoords.x, this.canvasH);
  }

  resize() {
    this.calcGlobalValues();
    this.setCanvasSize();
    this.calcElementsValues();
  }
}
const heroCanvas = new HeroCanvas();

export default heroCanvas;