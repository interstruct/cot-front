import Swiper from 'swiper/js/swiper';
import { throttle, debounce } from 'throttle-debounce';
import { DOM, BREAKPOINTS } from '../helpers/_consts';
import isTouch from '../helpers/_detectTouch';

export default () => {

  const $sliders = $('.js-goals-slider-container');
  const $sliderWrapper = $sliders.find('.js-goals-slider-wrapper');
  const $slides = $sliders.find('.js-goals-slider-slide');
  const $pagination = $sliders.find('.js-goals-slider-pagination');
  let swiper;
  let isInited = false;

  const addSliderClass = () => {
    $sliders.addClass('swiper-container');
    $sliderWrapper.addClass('swiper-wrapper');
    $slides.addClass('swiper-slide');
  };

  const removeSliderClass = () => {
    $sliders.removeClass('swiper-container');
    $sliderWrapper.removeClass('swiper-wrapper');
    $slides.removeClass('swiper-slide');
  };

  const initSlider = () => {
    if(isInited === true) return;
      addSliderClass();
      swiper = new Swiper('.js-goals-slider-container', {
        simulateTouch: false,
        pagination: {
          el: $pagination,
          clickable: true
        },
        slidesPerView: 1
      });

    isInited = true;
  };

  const destroySlider = () => {
    if(isInited === false) return;
    swiper.destroy(true, true);
    removeSliderClass();
    isInited = false;
  }

  const onResize = () => {
    const windowW = DOM.$win.innerWidth();
    if(windowW <= BREAKPOINTS.mobile){
      initSlider();
    }else{
      destroySlider();
    }
  }

  onResize();
  if(isTouch()){
    DOM.$win.on('orientationchange', debounce(300, onResize) );
  }else{
    DOM.$win.resize(throttle(300, onResize));
  }

};
