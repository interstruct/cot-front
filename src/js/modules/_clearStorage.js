export const clearNewsStorage = () => {
  sessionStorage.setItem('filterNewsActive', null);
}
export const clearTopicsStorage = () => {
  sessionStorage.setItem('filterTopicsActive', null);
}
