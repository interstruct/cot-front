import ScrollMagic from 'scrollmagic';

import initHeaderScrolling from './_outScene';

class Scroller {
  constructor() {
    this.controller = new ScrollMagic.Controller();
  }

  init() {
    initHeaderScrolling(this.controller);
  }
};

const scroller = new Scroller();
export default scroller;