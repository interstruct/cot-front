import ScrollMagic from 'scrollmagic';

import initHeaderScrolling from './_outScene';
import initContactStickyFormScene from './_contactStickyFormScene';

class Scroller {
  constructor() {
    this.controller = new ScrollMagic.Controller();
  }

  init() {
    initHeaderScrolling(this.controller);
    initContactStickyFormScene(this.controller);
  }
};

const scroller = new Scroller();
export default scroller;