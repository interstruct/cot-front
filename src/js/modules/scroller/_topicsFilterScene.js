import ScrollMagic from 'scrollmagic';

import { DOM, IS_PHONE, CLASSES } from '../../helpers/_consts';
import calcHeaderStickyHeight from '../../helpers/_calcHeaderStickyHeight';

const spacer = 0;
// const spacer = 96 + 30;

export default (controller) => {
  const $block = $('.js-latest-section');
  const { $out } = DOM;


  if (!$block[0] || IS_PHONE === true) return;

  const $navWrap = $block.find('.js-latest-nav-wrap');
  const $nav = $block.find('.js-latest-nav');
  const $gridItem = $('.js-grid-item').eq(0);
  const $content = $('.js-latest-content');
  let windowScroll = window.pageYOffset;
  let prevWindowScroll = window.pageYOffset;
  let gridItemsPB = parseInt($gridItem.css('padding-bottom'), 10) + spacer;
  let filterNavTopOffset = $navWrap.offset().top;
  // console.log(gridItemsPB)

  let navH = $nav.innerHeight();
  let headerH = calcHeaderStickyHeight();

  const calcOutHeight = () => {
    return $out.outerHeight();
  };

  const initStickyNav = () => {
    const duration = calcOutHeight()
    const scene = new ScrollMagic.Scene({
      triggerElement: $out[0],
      triggerHook: 0,
      reverse: true,
      duration,
      offset: 0
    })
      .addTo(controller);

    const updateValues = () => {
      gridItemsPB = parseInt($gridItem.css('padding-bottom'), 10) + spacer;
      filterNavTopOffset = $navWrap.offset().top;
      navH = $nav.innerHeight();
      headerH = calcHeaderStickyHeight();
      scene.duration(calcOutHeight());
    };

    scene.on("shift, update", () => {
      updateValues()
    });

    scene.on("progress", () => {
      windowScroll = window.pageYOffset;
      const direction = controller.info().scrollDirection;
      const contentHeight = $content.innerHeight();
      const contentBottomPos = $content.offset().top + contentHeight;

      if (direction === 'REVERSE') {
        // console.log(`REVERSE`)
        $nav.removeClass(CLASSES.active);

        const pos = windowScroll + headerH;
        if (pos <= filterNavTopOffset) {
          $nav.addClass('is-no-transform-transition');
          $nav.removeAttr('style');
          $nav.removeClass(CLASSES.fixed);
          $nav.addClass(CLASSES.active);
        } else if(pos > filterNavTopOffset){
          if (windowScroll + filterNavTopOffset <= contentBottomPos + gridItemsPB ) {
            $nav.removeClass('is-no-transform-transition');
            $nav.css({
              position: 'fixed',
              transform: `translateY(${headerH}px)`
            });
          }
        }

      } else if (direction === 'FORWARD') {
        // console.log(`FORWARD`)
        if(Math.abs(windowScroll - prevWindowScroll) < 2) return;
        $nav.removeClass('is-no-transform-transition');

        const pos = windowScroll + filterNavTopOffset - gridItemsPB;
        if (pos > contentBottomPos) {
          $nav.css({
            transform: `translateY(${-navH}px)`
          });
        } else  if (pos <= contentBottomPos){
          if (windowScroll > filterNavTopOffset) {
            $nav.removeClass('is-no-transform-transition');
            $nav.addClass(CLASSES.fixed);
            $nav.removeClass(CLASSES.active);
            $nav.css({
              position: 'fixed',
              transform: `translateY(${0}px)`
            });
          }
        }

      } else if (direction === 'PAUSED') {
        // console.log(`PAUSED`)
        if (windowScroll + filterNavTopOffset - gridItemsPB > contentBottomPos) {
          $nav.css({
            transform: `translateY(${-navH}px)`
          });
        }
      }
      prevWindowScroll = windowScroll;
    });
  };

  setTimeout(() => {
    initStickyNav();
  }, 100);

};
