import ScrollMagic from 'scrollmagic';
import { debounce } from 'throttle-debounce';

import { DOM, CLASSES, BREAKPOINTS, IS_TOUCH } from '../../helpers/_consts';
import getHeaderHeight from "../../helpers/_calcHeaderHeight";

// import {closeAllHeaderDrops} from "../_header-nav";

export default (controller) => {
  const { $out } = DOM;
  const {$headerWrapper} = DOM;
  const { $header } = DOM;
  const $headerInner = $header.find('.js-header-inner');
  const $headeSticky = $('.js-header-sticky');
  let headerInnerPT = DOM.$win.innerWidth() > BREAKPOINTS.mobile ? 16 : 8;
  const outHeight = $out.outerHeight();
  let windowScroll = window.pageYOffset;
  let prevWindowScroll = window.pageYOffset;
  let headerPT = parseFloat($headerInner.css('padding-top')) - headerInnerPT;
  let headerH = getHeaderHeight();

  const checkInitialState = () =>{
    if (windowScroll  <= headerH) {

      $headerWrapper.removeClass('is-show-reverse').addClass('is-no-transition is-top-position');
      $headeSticky.css({
        'position': 'absolute',
        'transform': `translateY(${headerPT}px)`
      })
    }else{
      $headerWrapper.removeClass('is-top-position')
    };
  }
  checkInitialState()

  const scene = new ScrollMagic.Scene({
    triggerElement: $out[0],
    reverse: true,
    duration: outHeight
  })
    .addTo(controller);

  scene.on("shift", () => {
    scene.duration($out.outerHeight())
  });

  scene.on('progress', () => {
    if (DOM.$body.hasClass(CLASSES.menu)) return;
    windowScroll =  window.pageYOffset;

    headerInnerPT = DOM.$win.innerWidth() > BREAKPOINTS.mobile ? 16 : 8;
    headerH = getHeaderHeight();
    headerPT = parseFloat($headerInner.css('padding-top')) - headerInnerPT;
    const direction = controller.info().scrollDirection;

    if (direction === 'REVERSE') {
      if (windowScroll  > headerPT) {
        $headerWrapper.addClass('is-show-reverse');
      } else {
        $headerWrapper.removeClass('is-show-reverse').addClass('is-no-transition is-top-position');
        $headeSticky.css({
          'position': 'absolute',
          'transform': `translateY(${headerPT}px)`
        })
      };
    } else if(direction === 'FORWARD') {
      if(Math.abs(windowScroll - prevWindowScroll) < 2) return;
      if(windowScroll > headerH){
        $headeSticky.removeAttr('style')
        $headerWrapper.removeClass('is-top-position is-show-reverse ');

        setTimeout(() => {
          $headerWrapper.removeClass('is-no-transition ');
        }, 50)
      }
    }
    prevWindowScroll = windowScroll;
  });

  const onResize = () => {
    checkInitialState()
  };

  if(IS_TOUCH)  DOM.$win.on('orientationchange', debounce(400, onResize));
};
