import ScrollMagic from 'scrollmagic';

import initHeaderScrolling from './_outScene';
import initPrinciplesScene from './_principlesStickyNavScene';
import initHeroScene from './_aboutHeroScene';

class Scroller {
  constructor() {
    this.controller = new ScrollMagic.Controller();
  }

  init() {
    initHeaderScrolling(this.controller);
    initHeroScene(this.controller);
    initPrinciplesScene(this.controller);
  }
};

const scroller = new Scroller();
export default scroller;