import ScrollMagic from 'scrollmagic';

import { DOM, CLASSES } from '../../helpers/_consts';
import inViewport from '../../helpers/_checkInViewport';

export default (controller) => {
  const { $out } = DOM;
  const $hero = $('.js-hero');
  const $heroBgShapes = $hero.find('.js-hero-bg-shapes');
  const outHeight = $out.outerHeight();

  const scene = new ScrollMagic.Scene({
    triggerElement: $out[0],
    reverse: true,
    duration: outHeight
  })
    .addTo(controller);
    
  scene.on("shift", () => {
    scene.duration($out.outerHeight());
  });

  scene.on('progress', () => {
    const heroInViewport = inViewport($hero);
    if(heroInViewport){
      $heroBgShapes.addClass(CLASSES.visible);
    }else{
      $heroBgShapes.removeClass(CLASSES.visible);
    }
  });
};