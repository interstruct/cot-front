import ScrollMagic from 'scrollmagic';
import { CLASSES } from '../../helpers/_consts';

export default (controller) => {
  const $block = $('.js-hero-wrapper');
  if (!$block[0]) return;

  const $banner = $('.banner-nl-wrapper');
  $banner.addClass(CLASSES.active);

  const scene = new ScrollMagic.Scene({
    triggerElement: $block[0],
    triggerHook: 1,
    reverse: false
  })
    .addTo(controller);
  scene.on("start, enter", () => {
    setTimeout(() => {
      $banner.addClass(CLASSES.show);
    }, 1500)
  });
};
