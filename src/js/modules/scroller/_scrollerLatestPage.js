import ScrollMagic from 'scrollmagic';

import initHeaderScrolling from './_outScene';
import initLatestFilterScene from './_latestFilterScene';

class Scroller {
  constructor() {
    this.controller = new ScrollMagic.Controller();
  }

  init() {
    initHeaderScrolling(this.controller);
    initLatestFilterScene(this.controller);
  }
};

const scroller = new Scroller();
export default scroller;