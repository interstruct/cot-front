import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';

import { debounce } from 'throttle-debounce';
import { TimelineMax, TweenMax, Power0 } from 'gsap';

import { DOM, BREAKPOINTS, GLOBAL, CLASSES, IS_TOUCH } from '../../helpers/_consts';
import isPortrait from '../../helpers/_isPortrait';
import isBigScreen from '../../helpers/_isBigScreen';

const rangeValue = (x, inMin, inMax, outMin, outMax) => { return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin };

export default (controller) => {
  const $block = $('.js-about-hero');
  if (!$block[0]) return;

  const setStaticView = () => {
    $block.addClass('is-static-view');
    $block.addClass(CLASSES.visible);
  };

  const setDinamicView = () => {
    const isSafari = GLOBAL.browser === 'safari';
    const isClip = GLOBAL.browser === 'firefox';
    let isNativeSticky = false;

    if (GLOBAL.browser === 'firefox' || GLOBAL.browser === 'chrome' || isSafari) isNativeSticky = true;

    const $StickyContainer = $block.find('.js-about-hero-sticky-container');
    const $StickyEl = $block.find('.js-about-hero-sticky-elem');
    const $circlesGroup = $block.find('.js-logo-circles');
    const $circles = $circlesGroup.find('circle');
    const $lines = $block.find('.js-logo-lines line');
    const $bg = isSafari ? $block.find('.js-about-hero-bg') : $block.find('.js-about-hero-clip-bg');

    let scaleFactor;
    let windowW;
    let startScale;
    let rangeWidth;
    let duration;
    let mainTl;
    let isPort;
    let isBigDisplay = isBigScreen();
    let $mask = $block.find('.js-logo-mask');

    if (isNativeSticky) DOM.$body.addClass('is-native-sticky');
    if (isClip) $mask = $block.find('.js-logo-clip-mask');

    const calcBreakpointValues = () => {
      if (windowW > BREAKPOINTS.desktop) {
        rangeWidth = 1680;
        scaleFactor = isPort ? 12 : 10;
      } else {
        rangeWidth = 1240;
        scaleFactor = 10;
      }
    }

    const calcValues = () => {
      isPort = isPortrait();
      isBigDisplay = isBigScreen();
      windowW = DOM.$win.innerWidth();
      duration = $StickyContainer.innerHeight() - $StickyEl.innerHeight() - window.innerHeight * 0.25;

      calcBreakpointValues();

      startScale = rangeValue(scaleFactor, 0, rangeWidth, 0, windowW);
    };
    calcValues();

    const prepareAnimationState = () => {
      TweenMax.set($mask, { transformOrigin: '50% 50%', scale: startScale });
      if (!isClip) {
        if (!isSafari) {
          if (isBigDisplay) {
            TweenMax.set($circlesGroup, { transformOrigin: '50% 50%', scale: 1.8 });
          } else {
            TweenMax.set($circlesGroup, { transformOrigin: '50% 50%', scale: 1.5 });
          }
          TweenMax.set($lines, { css: { strokeWidth: 15 } });
          TweenMax.set($circles, { css: { strokeWidth: 2 } });
        }else{
          TweenMax.set($bg, { opacity: 1 });
        }
      };
    };

    prepareAnimationState();

    const setMainTl = () => {
      if (isClip) {
        mainTl = new TimelineMax({
          onReverseComplete: () => {
            TweenMax.to($bg, 0.2, { opacity: 1, ease: Power0.easeNone }, 0)
          },
        });

        mainTl.to($bg, 0.2, { opacity: 0, ease: Power0.easeNone }, 0);
        mainTl.to($mask, 2,
          {
            scale: 1,
            ease: Power0.easeNone
          }, 0);
      } else {
        if (isSafari) {
          mainTl = new TimelineMax({
            onReverseComplete: () => {
              TweenMax.to($bg, 0.2, { opacity: 1, ease: Power0.easeNone }, 0)
            },
          })
            .to($bg, 0.2, { opacity: 0, ease: Power0.easeNone }, 0)
        } else {
          mainTl = new TimelineMax();
        }
        mainTl.to($mask, 2,
          {
            scale: 1,
            transformOrigin: '50% 50%',
            ease: Power0.easeNone
          }, 0);

        if (!isSafari) {
          mainTl
            .to($circlesGroup, 2,
              {
                scale: 1,
                ease: Power0.easeNone
              }, 0)
            .to($lines, 2,
              {
                css: { strokeWidth: 6 },
                ease: Power0.easeNone
              }, 0)
            .to($circles, 2,
              {
                css: { strokeWidth: 7 },
                ease: Power0.easeNone
              }, 0);
        }
      }
    };

    setMainTl();

    // eslint-disable-next-line
    const scene = new ScrollMagic.Scene({
      triggerElement: $StickyContainer[0],
      triggerHook: 0,
      duration,
      reverse: true,
    });

    if (!isNativeSticky) scene.setPin($StickyEl[0]);

    scene.setTween(mainTl);
    scene.addTo(controller);

    const onResize = () => {
      mainTl.kill();
      calcValues();
      prepareAnimationState();
      setMainTl();
      scene.duration(duration);
      scene.setTween(mainTl);
      scene.update();
    };

    DOM.$win.resize(debounce(450, onResize));

    if (isClip || GLOBAL.browser === 'edge') {
      setTimeout(() => {
        $block.addClass(CLASSES.visible);
      }, 300);
    } else if(isSafari){
        setTimeout(() => {
          $block.addClass(CLASSES.visible);
        }, 300);
      }else{
        $block.addClass(CLASSES.visible);
      }
  };

  if (GLOBAL.os === 'iOS' || GLOBAL.os === 'Android OS' || GLOBAL.os === 'Mac OS' && IS_TOUCH || GLOBAL.browser === 'ie' || GLOBAL.browser === 'firefox') {
    setStaticView();
  } else {
    // DOM.$body.addClass('is-native-sticky');
    setDinamicView();
  };
};
