import ScrollMagic from 'scrollmagic';
import { debounce } from 'throttle-debounce';

import { DOM, IS_PHONE,IS_TOUCH } from '../../helpers/_consts';
import calcHeaderStickyHeight from '../../helpers/_calcHeaderStickyHeight';

const ADDITIONAL_STICKY_SPACER = 32;
export default (controller) => {
  const $row = $('.js-contact-row');

  if (!$row[0] || IS_PHONE === true) return;
  const $form = $row.find('.js-contact-form-wrap');
  const $content = $row.find('.js-contact-content');

  const initStickyForm = () => {
    const headerHeight = calcHeaderStickyHeight();

    const calcDuration = () => {
      return $content.innerHeight() - $form.innerHeight();
    };

    const calcTriggerPos = () => {
      return ADDITIONAL_STICKY_SPACER / window.innerHeight;
    };

    const duration = calcDuration();
    const triggerPos = calcTriggerPos();

    const scene = new ScrollMagic.Scene({
      triggerElement: $row[0],
      triggerHook: triggerPos,
      reverse: true,
      duration,
      offset: -headerHeight
    })
      .setPin($form[0])
      .addTo(controller);
      
    scene.on("shift", () => {
      scene.duration(calcDuration());
      scene.triggerHook(calcTriggerPos());
    });

    const onResize = () => {
      scene.duration(calcDuration());
      scene.triggerHook(calcTriggerPos());
    };

    if(IS_TOUCH){
      DOM.$win.on('orientationchange', debounce(350, onResize));
    }else{
      DOM.$win.resize(debounce(200, onResize));
    }
  };

  setTimeout(() => {
    initStickyForm();
  }, 100);
};