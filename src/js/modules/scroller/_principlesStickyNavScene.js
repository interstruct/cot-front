import ScrollMagic from 'scrollmagic';
import { debounce } from 'throttle-debounce';

import { DOM, CLASSES, IS_PHONE, IS_TOUCH } from '../../helpers/_consts';
import calcHeaderStickyHeight from '../../helpers/_calcHeaderStickyHeight';
import inViewport from '../../helpers/_checkInViewport';

const navLinksClass = 'js-principle-nav-link';
const principlesClass = 'js-principle-block';
export default (controller) => {
  const $row = $('.js-principles-row');

  if (!$row[0] || IS_PHONE === true) return;
  const $nav = $row.find('.js-principles-nav-wrap');
  const $navLinks = $nav.find(`.${navLinksClass}`);
  const $content = $row.find('.js-principles-content');
  const $contentItems = $content.find(`.${principlesClass}`);

  const initStickyNav = () => {
    const headerHeight = calcHeaderStickyHeight();

    const calcDuration = () => {
      return $content.innerHeight() - $nav.innerHeight();
    };

    const duration = calcDuration();
    const scene = new ScrollMagic.Scene({
      triggerElement: $row[0],
      triggerHook: 0,
      reverse: true,
      duration,
      offset: -headerHeight
    })
      .setPin($nav[0])
      .addTo(controller);
    scene.on("shift", () => {
      scene.duration(calcDuration());
    });
  };
 
  const initStickyActiveNavLink = () => {
    const calcDuration = () => {
      return DOM.$out.outerHeight()
    };

    const setActiveNavItem = (direction) => {
      let direct = '';
      if (direction === 'REVERSE') {
        direct = 'upd';
      } else if (direction === 'FORWARD') {
        direct = 'down';
      } else {
        return;
      }

      $contentItems.each((i, el) => {
        const $el = $(el);
        const inView = inViewport($el, direct);

        if (inView) {
          const index = $el.attr('data-index');
          $navLinks.removeClass(CLASSES.active);
          $(`.${navLinksClass}[href="${index}"]`).addClass(CLASSES.active);
          return false;
        }
        return true;
      })
    };
    const outHeight = calcDuration();
    const scene = new ScrollMagic.Scene({
      triggerElement: DOM.$out[0],
      reverse: true,
      duration: outHeight
    })
      .addTo(controller);
    scene.on("shift", () => {
      scene.duration(calcDuration())
    });
    scene.on("progress", () => {
      const direction = controller.info().scrollDirection;
      setActiveNavItem(direction);

    });

    const onResize = () => {
      scene.duration(calcDuration());
    };

    if(IS_TOUCH){
      DOM.$win.on('orientationchange', debounce(350, onResize));
    }else{
      DOM.$win.resize(debounce(200, onResize));
    }
  };
  
  setTimeout(() => {
    initStickyNav();
  }, 100);
  initStickyActiveNavLink();
};