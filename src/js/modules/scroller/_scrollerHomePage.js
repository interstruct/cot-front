import ScrollMagic from 'scrollmagic';

import initHeaderScrolling from './_outScene';
import initHeroBgScene from './_clipScene';
import initLatestUpdatesScene from './_latestUpdatesScene';
// import initBannerNewsletterScene from './_bannerNewsletterScene';

class Scroller {
  constructor() {
    this.controller = new ScrollMagic.Controller();
  }

  init() {
    initHeaderScrolling(this.controller);
    initHeroBgScene(this.controller);
    // initBannerNewsletterScene(this.controller)
    initLatestUpdatesScene(this.controller);
  }
};

const scroller = new Scroller();
export default scroller;
