import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import { TimelineMax,  Power1} from 'gsap';

export default (controller) => {
  const $block = $('.js-latest-updates-block');
  if(!$block[0]) return;
  
  const addContentAnimation = () => {
    const $sliderWrapper = $block.find('.js-latest-updates-wrapper');
    const $slider = $block.find('.js-latest-updates-slider');
    const animContent = new TimelineMax()
      .fromTo($slider, 0.5,
        {
          y: 100,
          opacity: 0,
          ease: Power1.easeInOut
        },
        {
          y: 0,
          opacity: 1,
          ease: Power1.easeInOut
        }, 0);
    // eslint-disable-next-line
    const scene = new ScrollMagic.Scene({
      triggerElement: $sliderWrapper[0],
      triggerHook: 0.8,
      reverse: false
    })
      .setTween(animContent)
      .addTo(controller);
  };
  addContentAnimation()

};