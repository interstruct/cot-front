
import { GLOBAL } from '../helpers/_consts';

const initMsScroll = () => {
  if (GLOBAL.browser === 'ie' || GLOBAL.browser === 'edge') {
    document.addEventListener('mousewheel', (event) => {
      event.preventDefault();
      const wd = event.wheelDelta;
      const csp = window.pageYOffset;
      window.scrollTo(0, csp - wd);
    });
  }
};

export default initMsScroll;