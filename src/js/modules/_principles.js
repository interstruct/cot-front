import scrollToFunc from '../helpers/_scrollToFunc';
import { IS_PHONE } from '../helpers/_consts';

const navLinksClass = 'js-principle-nav-link';
const principlesClass = 'js-principle-block';

export default () => {
  const $row = $('.js-principles-row');

  if (!$row[0] || IS_PHONE === true) return;
  const $navLinks = $row.find(`.${navLinksClass}`);

  const setDesktopClickEvents = () => {
    $navLinks.on('click', (e) => {
      e.preventDefault();
      const $link = $(e.currentTarget);
      const index = $link.attr('href');
      const $target = $(`.${principlesClass}[data-index="${index}"]`);
      scrollToFunc($target);
    });
  };

  if(!IS_PHONE){
    setDesktopClickEvents()
  }
};