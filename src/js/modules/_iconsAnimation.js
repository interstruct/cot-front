import lottie from 'lottie-web';

import { IS_TOUCH } from "../helpers/_consts";

const initIconsAnimation = () => {
  const $iconElements = $('.js-animated-icon');

  $iconElements.each((index, icon) => {
    const data = icon.getAttribute('data-icon');
    const params = {
      container: icon,
      renderer: 'svg',
      loop: false,
      autoplay: false,
      path: data
    };

    let canPlay = true;
    let timeOut;

    const anim = lottie.loadAnimation(params);

    const handleMouseEnter = () => {
      timeOut = setTimeout(() => {
        if ( !canPlay) return;
        canPlay = false;
        anim.play();        
      }, 200); 
    };

    const handleMouseLeave = () => {
      clearTimeout(timeOut)
    }

    const handleAnimComplete = () => {
      anim.stop();
      canPlay = true;
    }
    if (!IS_TOUCH) {
      icon.addEventListener('mouseenter', handleMouseEnter);
      icon.addEventListener('mouseleave', handleMouseLeave);
      anim.addEventListener('complete', handleAnimComplete);
    }
  });
};

export default initIconsAnimation;
