import { DOM } from '../helpers/_consts';
import scrollFunc from '../helpers/_scrollToFunc';

const thisPageClass = 'js-scroll-this-page';
const otherPageClass = 'js-scroll-other-page';

export default class ScrollTo {
  constructor() {
    this.links = $('a');
  }

  init() {
    this.linksLoop()
    this.setEvents();
    ScrollTo.detectStorageData();
  }

  linksLoop(){
    this.links.each((i, el)  => {
      const $link = $(el);
      const href = $link.attr('href');
      const hashPos = ScrollTo.checkHash(href);

      if(hashPos >= 0){
          const type = ScrollTo.checkLinkType(href, hashPos);
          ScrollTo.prepareLinkHtml(type, $link)
      }
    });
  }

  static prepareLinkHtml(type, link){
    if(type === 'this'){
      link.addClass(thisPageClass)
    }else{
      link.addClass(otherPageClass)
    }
  }

  static checkHash(url){
    return url.indexOf('#');
  }

  static checkLinkType(href, pos){
    const POS_FOR_SIMPLE_SCROLL = 2;
    let type = 'this';

    if(pos > POS_FOR_SIMPLE_SCROLL){
      const location = window.location.origin;
      const {pathname} = window.location;

      if(href.indexOf(location) >= 0){
        type = 'other';
      }

      if(href.indexOf(pathname) >= 0 && pathname !=='/' && pathname.length > 2){
        type = 'this';
      }

      if(href.indexOf(`${window.location.href}#`) >= 0){
        type = 'this';
      }
    }
    return type;
  }

  setEvents(){

    DOM.$body.on('click', `.${thisPageClass}`,  ScrollTo.clickHandleSimple.bind(this))
    DOM.$body.on('click', `.${otherPageClass}`,  ScrollTo.clickHandleOtherPage.bind(this))

  }

  static clickHandleSimple(e){
    e.preventDefault();
    const href = $(e.currentTarget).attr('href');
    const hashPos = href.indexOf('#');

    const $target = $(href.substr(hashPos));
    scrollFunc($target);
  }

  static clickHandleOtherPage(e){
    e.preventDefault();
    const $link = $(e.currentTarget)
    const href = $link.attr('href');
    const hashPos = href.indexOf('#');
    const scrollTarget = href.substring(hashPos + 1);
    const url = href.substring(0, hashPos);

    setTimeout(() => {
      window.sessionStorage.setItem('scrollOtherPage', scrollTarget);
      document.location.href = url;
  }, 50)
  }

  static detectStorageData(){
    if(!window.sessionStorage) return;
    const storageValue = window.sessionStorage.getItem('scrollOtherPage');
    if(storageValue !== null && storageValue !== 'null') {
      const $target = $(`#${storageValue}`)
      setTimeout(() =>{
        scrollFunc($target);
        ScrollTo.clearStorage()
      }, 300)
    }
  }

  static clearStorage(){
    window.sessionStorage.setItem('scrollOtherPage', null);
  }
}
