
import DetectIosVersion from '../helpers/_detectIosVersion';

export default () => {
  const $videoWrapper = $('.js-hero-video-wrapper');
  const $video = $videoWrapper.find('.js-hero-video');
  const versionString = `${DetectIosVersion()}`;

  if(versionString.slice(0, 2) === '11'){
    const posterSrc = $video.attr('data-poster-src')
    const img = `<img class=" js-hero-video-poster" src="${posterSrc}" alt="video-poster" />`
    $videoWrapper.append(img);
  }
};
