const getRandomInt = (max) => {
  return Math.floor(Math.random() * Math.floor(max));
}

export default () => {
  const $heroContentBlocks = $('.js-hero-content');

  if(!$heroContentBlocks[0]) return;

  const blocksCount = $heroContentBlocks.length;
  const activeIndex = getRandomInt(blocksCount);

  const $activeBlock = $heroContentBlocks.eq(activeIndex);
  $activeBlock.fadeIn(500, 'linear');
};
