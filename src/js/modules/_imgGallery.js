import Swiper from 'swiper/js/swiper';
import { debounce } from 'throttle-debounce';
import { DOM } from '../helpers/_consts';

export default () => {

  const $slider = $('.js-img-gallery');
  if (!$slider[0]) return;

  const $slides = $slider.find('.js-img-gallery-slide');
  const $img = $slides.eq(0).find('.img-gallery__img > img, div');
  const $pagination = $slider.find('.js-img-gallery-pagination');
  const slidesCount = $slides.length;
  
  // eslint-disable-next-line
  let swiper;

  const setPaginationPosition = () => {
    const pos = $img.innerHeight() - $pagination.innerHeight() - 12;
    $pagination.css('top', `${pos}px`)
  }

  const onResize = () => {
    setPaginationPosition();
  };

  const initSlider = () => {
    if (slidesCount === 1) return;
    swiper = new Swiper('.js-img-gallery', {
      pagination: {
        el: $pagination,
        clickable: true
      },
      slidesPerView: 1
    });

    setPaginationPosition();
    DOM.$win.on('resize', debounce(450, onResize));
  };

  initSlider();

};
