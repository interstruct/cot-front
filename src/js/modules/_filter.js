import Isotope from 'isotope-layout';

import { CLASSES } from '../helpers/_consts';

const filterBtnClasss = 'js-filter-btn';

const parseStringToJquery = (str) => {
  const array = str.split(', ');

  const $btns = array.reduce((previousValue, currentItem, index) => {

    if (index === 0) {
      return $(`.${filterBtnClasss}[data-filter="${currentItem}"]`);
    }
    return previousValue.add(`.${filterBtnClasss}[data-filter="${currentItem}"]`);

  }, 0);

  return $btns;
}

const initFilter = (scroller, filterType) => {
  let canChange = true;
  const $activeCount = $('.js-filter-active-count');
  const $totalCount = $('.js-filter-total-count');
  const $filterBtns = $(`.${filterBtnClasss}`);
  let storage = sessionStorage.getItem(filterType);
  const isStorageEmpty = !!(storage === null || storage === 'null' || storage === '*');

  if(storage === '*') storage = null;

  let filters = [];
  if(!isStorageEmpty) filters = storage.split(', ');

  const removeFromFilter = (item) => {
    const index = filters.indexOf(item);
    filters.splice(index, 1);
  };

  const addToFilter = (item) => {
    filters.push(item);
  };

  const getFilterString = () => {
    let string = filters.join(', ');
    string = string === '' ? '*' : string;
    return string
  };

  if (!isStorageEmpty) {
    const $activeBtns = parseStringToJquery(storage);
    $activeBtns.addClass(CLASSES.active);
  }

  const iso = new Isotope('.js-isotope-grid', {
    itemSelector: '.js-grid-item',
    percentPosition: true,
    masonry: {
      columnWidth: '.js-grid-item'
    },
    filter: getFilterString()
  });

  const updateFilterCount = () => {
    $activeCount.html(iso.filteredItems.length);
  };

  const updateTotalCount = () => {
    $totalCount.html(iso.items.length);
  };

  updateTotalCount();
  updateFilterCount();

  const handleFilterBtn = (e) => {
    if (canChange === false) return;
    canChange = false;

    const $btn = $(e.currentTarget);
    const value = $($btn).attr('data-filter');

    if ($btn.hasClass(CLASSES.active)) {
      $btn.removeClass(CLASSES.active);
      removeFromFilter(value);
    } else {
      $btn.addClass(CLASSES.active);
      addToFilter(value);
    }
    const filteredString = getFilterString();

    sessionStorage.setItem(filterType, filteredString);

    iso.arrange({
      filter: filteredString
    });

    iso.on('arrangeComplete', () => {
      setTimeout(() => {
        updateFilterCount();
        scroller.controller.update();
        canChange = true;
      }, 100)
    });
  };

  const handleFilterNavBtn = (e) => {
    const $nav = $(e.currentTarget).parents('.js-latest-nav');

    if ($nav.hasClass(CLASSES.active)) {
      $nav.removeClass(CLASSES.active);
    } else {
      $nav.addClass(CLASSES.active);
    }
  };

  $filterBtns.on('click', handleFilterBtn);
  $('.js-latest-nav-btn').on('click', handleFilterNavBtn);
};
export default initFilter;
