export const initFilterBtn =  () => {
  const $btn = $('.js-filter-btn');

  const handleClick = (e) => {
    e.preventDefault();
    const $button = $(e.currentTarget);
    const href = $button.attr('href');
    const filterActive = $button.attr('data-filter-active');

    sessionStorage.setItem('filterNewsActive', filterActive);
    document.location.href = href;
  };

  $btn.on('click', handleClick);
};


export const initNavFilterBtn =  (btn, filterType) => {
  const $btn = btn;

  const handleClick = (e) => {
    e.preventDefault();
    const $button = $(e.currentTarget);
    const href = $button.attr('href');
    const filterActive = $button.attr('data-filter-active');

    sessionStorage.setItem(filterType, filterActive);
    document.location.href = href;
  };

  $btn.on('click', handleClick);
};

export const initNavFilterBtns = () => {
  initNavFilterBtn($('.js-news-filter-link'), 'filterNewsActive');
  initNavFilterBtn($('.js-topics-filter-link'), 'filterTopicsActive');
}
