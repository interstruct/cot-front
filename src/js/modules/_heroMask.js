import { IS_PHONE, GLOBAL } from '../helpers/_consts';

const initHeroMask = () => {
if(GLOBAL.browser === 'ie') return;

  const circleMask = IS_PHONE ? document.getElementById("circle-mask-circle-mob"): document.getElementById("circle-mask-circle1");
  const scrollHandler = () => {
    const scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);

    circleMask.style.transform = `translateY(${scrollTop}px)`;
  };
  
  window.addEventListener('scroll', scrollHandler);
};

export default initHeroMask;