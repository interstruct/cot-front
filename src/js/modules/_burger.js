import { debounce } from 'throttle-debounce';
import { CLASSES, DOM, BREAKPOINTS } from '../helpers/_consts';
import {closeAllHeaderDrops, closeAllHeaderMobDrops} from "./_header-nav";

const $headerBurger = $('.js-header-burger');
const $headerNav = $('.js-header-mob-nav');
let navWinScrollTop = 0;
let isNavOpen = false;

const initBurger = (scroller) => {
  const closeBurger = () => {
    $headerBurger.removeClass(CLASSES.active);
      DOM.$body.removeClass(CLASSES.overflowed);

      scroller.controller.scrollTo(navWinScrollTop);
      isNavOpen = false;

      $headerNav[0].scrollTop = 0;

      setTimeout(() => {
        DOM.$body.removeClass(CLASSES.menu);
        $headerNav.removeClass(CLASSES.active);
      },50)
  }
  const handleBugerClick = () => {
    if($headerBurger.hasClass(CLASSES.active)){
      closeBurger();
    }else{
      navWinScrollTop = DOM.$win.scrollTop();

      $headerBurger.addClass(CLASSES.active);
      $headerNav.addClass(CLASSES.active);
      DOM.$body.addClass(CLASSES.overflowed);
      DOM.$body.addClass(CLASSES.menu);
      isNavOpen = true;

      $headerNav[0].scrollTop = 0;
    }
  };

  $('.js-page-next-scroll-link').on('click', () => {
    const ww = DOM.$body.innerWidth();
    const isMob = ww <= BREAKPOINTS.mobile;
    if(isMob) closeBurger();
    closeAllHeaderDrops();
    closeAllHeaderMobDrops();
  });

  function onNavResize(){
    if(DOM.$win.innerWidth() > BREAKPOINTS.mobile && isNavOpen === true){
      isNavOpen = false;
      $headerBurger.removeClass(CLASSES.active);
      $headerNav.removeClass(CLASSES.active);
      DOM.$body.removeClass(CLASSES.menu);
      DOM.$body.removeClass(CLASSES.overflowed);
      DOM.$win.scrollTop(navWinScrollTop);
    }
  }

  const debounceFunc = debounce(300, onNavResize)

  $headerBurger.on('click', handleBugerClick);
  DOM.$win.on('orientationchange', debounceFunc);
};

export default initBurger
