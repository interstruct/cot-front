import { CLASSES } from '../helpers/_consts';

const parentClass = 'js-input-state-animation';
const inputClass = 'js-input-state-animation-input'

const initInputAnimationsState = () => {
  $(`.${parentClass}`).each((i, el) => {
    const $wrap = $(el);
    const $input = $wrap.find(`.${inputClass}`);

    $input.focus(() => {
      $wrap.addClass(CLASSES.active);
    });
    // eslint-disable-next-line
    $input.blur(function(){
      const $el = $(this);
      const value = $el.val();
      if(value === ''){
        $wrap.removeClass(CLASSES.active);
      };
    });
  });
};

export default initInputAnimationsState;