import { TweenLite } from 'gsap/TweenMax';
import { throttle, debounce } from 'throttle-debounce';

import { CLASSES, DOM, BREAKPOINTS, IS_TOUCH, IS_PHONE } from '../helpers/_consts';
import scrollFunc from '../helpers/_scrollToFunc';

const $mainDropLinks = $('.js-header-main-link');
const $mainDropLinksWrappers = $('.js-header-main-link-wrapper');

const $mainMobDropLinks = $('.js-header-main-mob-link');
const $mainMobDropLinksWrappers = $('.js-header-main-mob-link-wrapper');

export const closeAllHeaderDrops = () => {
  $mainDropLinks.removeClass(CLASSES.active);
  $mainDropLinksWrappers.removeClass(CLASSES.active);
}
export const closeAllHeaderMobDrops = () => {
  $mainMobDropLinks.removeClass(CLASSES.active);
  $mainMobDropLinksWrappers.removeClass(CLASSES.active);
  $mainMobDropLinksWrappers.find('.js-header-mob-drop-menu').removeAttr('style')
}

const initAboutScrollLinks = () => {
  const isAboutPage = !!$('.js-about-hero')[0]
  const $links = $('.js-page-next-scroll-link');

  if(!isAboutPage) {
    $links.on('click', (e) => {
      e.preventDefault();

      const href = $(e.currentTarget).attr('href');
      const hashPos = href.indexOf('#');
      const targetID = href.substr(hashPos);
      const pageUrl = href.substr(0, hashPos)

      sessionStorage.setItem('scrollNextPage', targetID);
      document.location.href = pageUrl;
    })
  } else {
    const storage = sessionStorage.getItem('scrollNextPage');

    if(storage !== null && storage !== 'null') {
      const $target = $(`${storage}`)
      const isOffsetSection = $target.hasClass('about-page-wrapper');
      const calcOffset = isOffsetSection;

      setTimeout(() =>{
        scrollFunc($target, calcOffset, false, false, 1);
        sessionStorage.setItem('scrollNextPage', null);
      }, 800)
    }

    $links.on('click', (e) => {
      e.preventDefault();
      const ww = DOM.$body.innerWidth();
      const isMob = ww <= BREAKPOINTS.mobile;

      const href = $(e.currentTarget).attr('href');
      const hashPos = href.indexOf('#');
      const $target = $(href.substr(hashPos));
      const scrollDelay = isMob ? 200 : 0 ;
      const isOffsetSection = $target.hasClass('about-page-wrapper');
      const calcOffset = isOffsetSection;

      setTimeout(() => {
        scrollFunc($target, calcOffset, false, false,  0.7);
      }, scrollDelay)
    })
  };

}

const initHeaderNav = () => {
  const preventLinks = () => {
    $mainDropLinks.on('click', (e) => {
      e.preventDefault();
    });
    $mainMobDropLinks.on('click', (e) => {
      e.preventDefault();
    });
  }

  const initDesktopMenuHovers = () => {
    DOM.$win.on('scroll', throttle(50, closeAllHeaderDrops));

    if(IS_TOUCH) {
      $mainDropLinks.on('click', (e) => {
        e.preventDefault();

        const $link = $(e.currentTarget);


        if($link.hasClass(CLASSES.active)) {
          closeAllHeaderDrops();
        } else {
          const $parent = $link.parent();
          closeAllHeaderDrops();

          $link.addClass(CLASSES.active);
          $parent.addClass(CLASSES.active)
        }
      });

      $(document).on('click', (e) => {
        if ($(e.target).closest('.js-header-main-link-wrapper').length === 0) {
          closeAllHeaderDrops();
        }
      });
    } else {
      $mainDropLinksWrappers.mouseenter((e) => {
        e.preventDefault();

        const $parent = $(e.currentTarget);
        if($parent.hasClass(CLASSES.active)) {
          closeAllHeaderDrops();
        } else {
          closeAllHeaderDrops();
          $parent.addClass(CLASSES.active)
        }
      });
      $mainDropLinksWrappers.mouseleave(() => {
        closeAllHeaderDrops();
      });
    };
  }

  const openMenuBlockAnim = (block) => {
    const $inner = block.find('.js-header-drop-menu-inner');
    const innerHeight = $inner.innerHeight();
    TweenLite.to(block, 0.45, {height: innerHeight});
  }

  const closeMenuBlockAnim = (block) => {
    block.removeClass(CLASSES.active);
    TweenLite.to(block, 0.45, {height: 0});
  }

  const closeAllMobDrops = () => {
    $mainMobDropLinksWrappers.each((i, el) => {
      const $el = $(el);
      if($el.hasClass(CLASSES.active)) {
        const $dropMenu = $el.find('.js-header-mob-drop-menu')
        closeMenuBlockAnim($dropMenu)
        $el.removeClass(CLASSES.active);
      }
    })
  }

  const initMobMenu = () => {
    if(!IS_TOUCH) return;

    $mainMobDropLinks.on('click', (e) => {
      e.preventDefault();

      const $block = $(e.currentTarget).parent();
      const $dropMenu = $block.parent().find('.js-header-mob-drop-menu')


      if($block.hasClass(CLASSES.active)) {
        $block.removeClass(CLASSES.active);
        closeMenuBlockAnim($dropMenu)

      } else {
        closeAllMobDrops();
        openMenuBlockAnim($dropMenu)
        $block.addClass(CLASSES.active);
      }
    });
  }

  const debounceFunc = debounce(300, () => {
    const ww = DOM.$win.innerWidth();
    const wh = DOM.$win.innerHeight();
    if(ww > BREAKPOINTS.mobile || wh > BREAKPOINTS.mobile) {
      closeAllHeaderDrops();
      closeAllHeaderMobDrops();
    }
  })

  if(IS_TOUCH) {
    preventLinks();

    if(IS_PHONE) DOM.$win.on('orientationchange', debounceFunc);
  };

  initDesktopMenuHovers();
  initMobMenu();
  initAboutScrollLinks();
};

export default initHeaderNav
