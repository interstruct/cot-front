import Swiper from 'swiper/js/swiper';

export default () => {
  $('.js-latest-updates-slider').each((index, item) => {
    
    const $slider = $(item);
    const $slides = $slider.find('.js-latest-updates-slide');
    const $pagination = $slider.find('.js-latest-updates-slider-pagination');
    const slidesCount = $slides.length;
    
    const options = {
      simulateTouch: false,
      pagination: {
        el: $pagination,
        clickable: true
      },
      breakpoints: {
        1240: {
          slidesPerView: 3,
          spaceBetween: 0,
          simulateTouch: false
        },
        960: {
          spaceBetween: 58,
          slidesPerView: 2,
          simulateTouch: false,
        },
        720: {
          slidesPerView: 2,
          spaceBetween: 36,
          simulateTouch: false,
        },
        320: {
          slidesPerView: 1,
          simulateTouch: false
        }
      }
    };

    if(slidesCount === 2){
      $pagination.addClass('is-hide-on-tablet');
    } else if (slidesCount === 1){
      options.pagination = false;
      options.allowTouchMove = false;
    };

    // eslint-disable-next-line
    const swiper = new Swiper(item, options);
  })

};
