import Swiper from 'swiper/js/swiper';
import { throttle, debounce } from 'throttle-debounce';
import { DOM, BREAKPOINTS } from '../helpers/_consts';
import isTouch from '../helpers/_detectTouch';

export default () => {

  const $sliders = $('.js-partner-slider');
  if(!$sliders[0]) return;

  const $sliderWrapper = $sliders.find('.js-partner-slider-wrapper');
  const $slides = $sliders.find('.js-partner-slider-slide');
  const $navigation = {
    nextEl: $('.js-partner-slider-button-next'),
    prevEl: $('.js-partner-slider-button-prev'),
  };
  const $pagination = $sliders.find('.js-partner-slider-pagination');
  let swiper;
  let isInited = false;

  const addSliderClass = () => {
    $sliders.addClass('swiper-container');
    $sliderWrapper.addClass('swiper-wrapper');
    $slides.addClass('swiper-slide');
  };

  const removeSliderClass = () => {
    $sliders.removeClass('swiper-container');
    $sliderWrapper.removeClass('swiper-wrapper');
    $slides.removeClass('swiper-slide');
  };

  const initSlider = (spaceBetween, slides) => {
    if(isInited === true) return;
      addSliderClass();
      swiper = new Swiper('.js-partner-slider', {
        navigation: $navigation,
        pagination: {
          el: $pagination,
          clickable: true
        },
        slidesPerView: slides,
        slidesPerGroup: slides,
        spaceBetween: `${spaceBetween}%`,
      });

    isInited = true;
  };

  const destroySlider = () => {
    if(isInited === false) return;
    swiper.destroy(true, true);
    removeSliderClass();
    isInited = false;
  };

  const onResize = () => {
    destroySlider();
    const windowW = DOM.$win.innerWidth();
    if(windowW <= BREAKPOINTS.mobile){
      initSlider(1*100/12, 2);
    }
    else if (windowW <= BREAKPOINTS.tablet){
      initSlider(2*100/22, 3);
    }
    else if (windowW <= BREAKPOINTS.desktop){
      initSlider(1.5*100/18, 3);
    }
    else{
      initSlider(2*100/16, 3);
    }
  };

  onResize();
  if(isTouch()){
    DOM.$win.on('orientationchange', debounce(300, onResize) );
  }else{
    DOM.$win.resize(throttle(300, onResize));
  }
};
