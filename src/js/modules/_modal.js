import Swiper from 'swiper/js/swiper';
import { debounce } from 'throttle-debounce';

import Player from './_player';
import { CLASSES, DOM, GLOBAL } from '../helpers/_consts';

const modalClass = 'js-modal';
const $modalLinks = $('.js-modal-link');
const $closeBtn = $('.js-modal-close');
let scrollPos = 0;
const sliderClass = 'js-modal-slider';
let swiper;
let modalPlayer;

const $modalVideoWrap = $('.js-modal-video-wrap');

const createVideoStr = (videoData) => {
  return `<video disablePictureInPicture controls playsinline autoplay >
  <source src="${videoData.src}" type="video/${videoData.type}" />
</video>`;
};

const onSliderResize = () => {
  swiper.update();
};

const openModalSlider = ($modal, slideIndex) => {
  const $slider = $modal.find(`.${sliderClass}`);
  const $sliderPagination = $modal.find('.js-modal-slider-pagination');
  const sliderInited = $slider.attr('data-slider-inited');

  if (sliderInited === 'false') {
    $modal.addClass(CLASSES.active);
    swiper = new Swiper($slider, {
      simulateTouch: false,
      autoHeight: true,
      pagination: {
        el: $sliderPagination,
        clickable: true
      },
      slidesPerView: 1
    });
    swiper.slideTo(slideIndex - 1);
    $slider.attr('data-slider-inited', 'true');
    DOM.$win.on('orientationchange', debounce(850, onSliderResize));
  } else {
    swiper.slideTo(slideIndex - 1);
    $modal.addClass(CLASSES.active);
    swiper.update();
  }
};


const openModalVideo = ($modal, videoData) => {
  const videoStr = createVideoStr(videoData);

  $modalVideoWrap.html('');
  $modalVideoWrap.append(videoStr);

  if(GLOBAL.browser !== 'ie'){
    const $video = $modal.find('video');
    modalPlayer = new Player($video);
  }

  $modal.addClass(CLASSES.active);
};

const initModals = (scroller) => {
  const openModal = ($modal) => {
    $modal.addClass(CLASSES.active);
  };

  const closeModal = () => {
    DOM.$body.removeClass(CLASSES.overflowed);
    scroller.controller.scrollTo(scrollPos);

    if ($modalVideoWrap[0]){
      if(GLOBAL.browser !== 'ie'){
        modalPlayer.destroy();
      }
      $modalVideoWrap.html('');
    }; 

    $(`.${modalClass}`).removeClass(CLASSES.active);
  };

  const handleModalLinkClick = (e) => {
    e.preventDefault();

    const $link = $(e.currentTarget);
    const targetAttr = $link.attr('data-target');
    const type = $link.attr('data-type');
    const $target = $(`.${modalClass}[data-modal="${targetAttr}"]`);
    scrollPos = DOM.$win.scrollTop();

    DOM.$body.addClass(CLASSES.overflowed);

    if (type === 'slider') {
      const slideIndex = $link.attr('data-active-slide');
      openModalSlider($target, slideIndex);
    } else if (type === 'video') {
      const videoData = {
        src: $link.attr('data-video-src'),
        type: $link.attr('data-video-type')
      };
      openModalVideo($target, videoData);
    } else {
      openModal($target, $target);
    }
  };

  const handleCloseModal = (e) => {
    const $clickElem = $(e.currentTarget);
    const $modal = $clickElem.parents(modalClass);
    closeModal($modal);
  };

  $modalLinks.on('click', handleModalLinkClick);
  $closeBtn.on('click', handleCloseModal);
};

export default initModals;